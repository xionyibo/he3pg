# He3DB部署指南

## 1、前提

-  部署PostgreSQL数据库：作为he3fs的元数据库。

-   部署redis服务：用来储存数据页与wal的链表关系，目前访问redis的url需要指定为“redis://:VlJi7uBV@127.0.0.1:6379/0”。

 

## 2、部署存储服务He3FS

### 2.1 安装客户端

- 克隆源码

```shell
git clone https://gitee.com/he3db/he3fs.git
```

- 进入源代码目录

```shell
cd he3fs
```

- 在master分支下执行编译

```shell
make
```

编译好的he3fs二进制程序位于当前目录，然后把该文件放到/usr/local/bin目录下。

### 2.2 以单机模式运行

- 创建文件系统

创建文件系统使用客户端提供的format命令，格式为：

```shell
he3fs format --trash-days 0 [command options] META-URL NAME
```

目前he3fs的META-URL仅支持PostgreSQL，其他参数请参考[juicefs单机模式快速上手指南](https://juicefs.com/docs/zh/community/quick_start_guide)

- 挂载文件系统

挂载文件系统使用客户端提供的mount命令，格式为：

```shell
he3fs mount --redis-meta=redis://:VlJi7uBV@127.0.0.1:6379/0 --enable-clean --he3data=dirname [command options] MEAT-URL MOUNTPOINT
```

he3fs在juicefs参数的基础上，mount的时候增加了三个参数选项：

```
--redis-meta：表示存储数据页与wal链表关系的redis库连接串。
--enable-clean：表示允许该he3fs的客户端清理redis中的链表关系。
--he3data: 表示推进节点的数据目录的相对路径，默认值是data。
```

其余参数，请参考[juicefs单机模式快速上手指南](https://juicefs.com/docs/zh/community/quick_start_guide)



## 3、部署计算服务He3PG

目前仅支持he3pg的主节点与推进节点部署在同一台机器，并且部署he3pg时需要以非root用户部署。

### 3.1 安装

- Requirements

  he3pg与PostgreSQL的编译要求是相同的，请参考[Requirements](https://www.postgresql.org/docs/current/install-requirements.html)

- 克隆源码

```shell
useradd postgres
su - postgres
git clone https://gitee.com/he3db/he3pg.git
```

- 进入源代码目录

```shell
cd he3pg
```

- 在master分支下执行编译

```shell
./configure
make
make install
```

其中，configure选项参考[CONFIGURE-OPTIONS](https://www.postgresql.org/docs/current/install-procedure.html#CONFIGURE-OPTIONS)

### 3.2 配置主机环境变量

例：在执行configure命令时，设置--prefix参数为/home/postgres/psql14，则设置以下参数：

```shell
export LD_LIBRARY_PATH=/home/postgres/psql14/lib
export PGHOME=/home/postgres/psql14
export PATH=$PGHOME/bin:/home/postgres:$PATH
```

### 3.3 初始化数据

在第二步部署存储服务时，以挂载文件系统时指定的挂载点为/data/jfspg为例

```shell
mkdir ~/he3data
rm -rf ~/he3data/*
initdb -D  /data/jfspg/pg    //子目录pg为he3fs mount中--he3data配置的目录
cd /data/jfspg/
rsync -av --exclude pg/base --exclude pg/global --exclude pg/pg_wal --exclude pg/pg_tblspc pg ~/he3data                               //拷贝非共享数据给主目录
cd initdata		
cp -r pushconf/* /data/jfspg/pg/     //拷贝推进配置到共享存储目录，用于启动推进节点			
cp -r masterconf/* ~/he3data/pg/          //拷贝主配置数据到本地主库数据目录，用于启动主节点
ln -sf /data/jfspg/pg/base ~/he3data/pg/base
ln -sf /data/jfspg/pg/global ~/he3data/pg/global
ln -sf /data/jfspg/pg/pg_wal ~/he3data/pg/pg_wal
ln -sf /data/jfspg/pg/pg_tblspc ~/he3data/pg/pg_tblspc			//主节点与推进节点共享base、global、pg_wal、pg_tblspc这四个目录
```

### 3.4 启动主节点

```shell
pg_ctl start -D ~/he3data/pg  
psql -p15432 //在主节点操作
create role repl with replication login password '123456'; //创建流复制用户，如果不创建推进无法启动
```

### 3.5 启动推进节点

```shell
pg_ctl start -D  /data/jfspg/pg/
psql -p15431  //登录推进可以查看主操作的数据
```



