#ifndef PUSH_PAGE_H
#define PUSH_PAGE_H
#include <time.h>
#include "c.h"
#include "access/xlogdefs.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "access/hiredis.h"
#include "common/relpath.h"
#include "storage/block.h"
#include "catalog/pg_control.h"

extern clock_t start_time;
extern XLogRecPtr	PushPtr;
extern XLogRecPtr PrePushPtr;
extern pid_t startupPid;
extern CheckPoint GlobalCheckPoint;
extern uint8 GlobalState;
extern XLogRecPtr  ApplyLsn;


extern XLogRecPtr QueryMinLsn(XLogRecPtr lsn);
extern XLogRecPtr QueryPushLsn();
extern XLogRecPtr QueryPushChkpointLsn();

typedef struct DirtyPage {
	XLogRecPtr startlsn;
	XLogRecPtr endlsn;
	Oid			dbNode;			/* database */
	Oid			relNode;		/* relation */
	ForkNumber forkNum;
	BlockNumber blockNum;
} DirtyPage;

//创建队列结构
typedef DirtyPage QDataType; 
 //创建队列节点
typedef struct QueueNode
{
    QDataType data; //存储数据
    struct QueueNode* next; //记录下一个节点
}QNode;
 //保存队头和队尾
typedef struct Queue
{
    QNode* head; //头指针
    QNode* tail; //尾指针
}Queue;

extern Queue DirtyPq;
  
//入队列
extern void QueuePush(QDataType x);
  
//出队列
extern QDataType QueuePop();

extern bool QueueEmpty();

extern bool pushRedisList(const char*str);

extern XLogRecPtr QueueHeadEndLsn();

#endif

