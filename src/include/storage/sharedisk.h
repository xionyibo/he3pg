#ifndef SHAREDISK_H
#define SHAREDISK_H
#include "c.h"
typedef enum ShareDiskMode
{
	SHARE_PRIMARY,
	SHARE_STANDBY,
	SHARE_UNKOWN			
} ShareDiskMode;
#define SHARE_DISK_NAME_LEN      64  
/* struct for storing share disk information */
typedef struct ShareDiskInfo
{
	int64			vdl;
	int64	        vcl;
	char            name[SHARE_DISK_NAME_LEN];
	ShareDiskMode   state;
}ShareDiskInfo;

extern Size ShareDiskShmemSize(void);
extern void ShareDiskShmemInit(void);

#endif

