/*-------------------------------------------------------------------------
 *
 * pg_hot_data.h
 *        definition of the "hot_data" system catalog (pg_hot_data)
 *
 *
 * Portions Copyright (c) 2022, He3DB Global Development Group
 *
 * src/include/catalog/pg_hot_data.h
 *
 * NOTES
 *        The Catalog.pm module reads this file and derives schema
 *        information.
 *
 *-------------------------------------------------------------------------
 */
#ifndef PG_HOT_DATA_H
#define PG_HOT_DATA_H

#include "catalog/genbki.h"
#include "catalog/pg_hot_data_d.h"

/* ----------------
 *              pg_hot_data definition.  cpp turns this into
 *              typedef struct FormData_pg_hot_data
 * ----------------
 */
CATALOG(pg_hot_data,4790,HotDataRelationId) BKI_SHARED_RELATION BKI_ROWTYPE_OID(4793,HotDataRelation_Rowtype_Id) BKI_SCHEMA_MACRO
{
    /* database name */
    NameData        datname;

    /* relation name */
    NameData        relname;

    /* caching rules */
    char    crules;

    /* client name */
    NameData        clientname;

    /* client addr */
    NameData	clientaddr;

#ifdef CATALOG_VARLEN			/* variable-length fields start here */
    /* cache rules schedule time */
    timestamptz crulessettime;
    
    /* hot data cache time */
    timestamptz cachetime;
#endif
}FormData_pg_hot_data;

/* ----------------
 *              Form_pg_hot_data corresponds to a pointer to a tuple with
 *              the format of pg_hot_data relation.
 * ----------------
 */
typedef FormData_pg_hot_data *Form_pg_hot_data;

DECLARE_UNIQUE_INDEX(pg_hot_data_datname_relname_index, 4791, on pg_hot_data using btree(datname name_ops, relname name_ops));
#define HotDataDatnameRelnameIndexId  4791

extern void PrecacheHotData();

#endif